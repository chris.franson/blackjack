import random

deck = []
player_hand = []
dealer_hand = []
bank = 0
bet_amount = 0
player_total = 0


def createDeck():
    global deck
    suit=["D","H","C","S"]
    rank=["2","3","4","5","6","7","8","9","10","J","Q","K","A"]
    for s in suit:
        for r in rank:
            deck.append(r+s)


def printDeck():
    global deck
    for d in deck:
        print(d)


def shuffleDeck():
    global deck
    random.shuffle(deck)


def main():
    global bank
    cardDraw = 0
    bank = 50
    createDeck()
    shuffleDeck()
    print("\n\nWelcome to the blackjack table!\n")
    menu()


def menu():
    global deck, bank, player_hand, dealer_hand, bet_amount
    print("\nYour balance is $" + str(bank) + "\n")
    if bank <= 0:
        print("You are out of money :(")
        exit()
    print("Options:")
    print("1 - Play")
    print("2 - Quit")
    print("\n")

    choice = int(input("Choose your option: "))
    if choice == 1:
        if bank > 0:
            # New round
            bet_amount = int(input("What's your bet? $"))
            while bet_amount > bank:
                print("You can't cover that bet.")
                bet_amount = int(input("Enter a value less than or equal to " + str(bank) + ": $"))
            deal(player_hand)
            deal(player_hand)
            deal(dealer_hand)
            deal(dealer_hand)
            player_total = get_status(True, True)
            if player_total == 21:
                print("YOU WIN on the first deal! You get 1.5x your bet.\n")
                bank += round(bet_amount*1.5)
                reset_game()
                menu()
            round_menu()
        else:
            print("You are out of money :(")
            exit()
    else:
        exit()


def round_menu():
    global player_hand, dealer_hand, bank, bet_amount, player_total
    print("Options:")
    print("1 - Hit")
    print("2 - Stand")
    print("3 - Quit")
    choice = int(input("\nchoose your option: "))
    if choice == 1:
        deal(player_hand)
        player_total = get_hand_total(player_hand, "player")
        get_status(True, False)
        if player_total == 21:
            print("\nYOU WIN! :D\n")
            bank += bet_amount*2
            reset_game()
            menu()
        elif player_total > 21:
            print("\nYOU LOSE! :(\n")
            bank -= bet_amount
            reset_game()
            menu()
        else:
            round_menu()
    elif choice == 2:
        # Now it's the dealer's turn
        print("Dealer's turn.\n")
        get_status(False, False)
        dealer_total = get_hand_total(dealer_hand, "dealer")
        while dealer_total <= 16:
            print("Dealer hits.\n")
            deal(dealer_hand)
            dealer_total = get_hand_total(dealer_hand, "dealer")
        get_status(False, False)
        check_winner()
    else:
        print("Thanks for playing! Bye!")
        exit()


def deal(hand):
    card = deck.pop()
    hand.append(card)


def get_hand_total(hand, hand_type, skip_aces=False):
    total = 0
    for card in hand:
        if skip_aces and card[0] == "A":
            continue
        else:
            total += get_card_value(card, hand_type)
    return total


def get_card_value(card, hand_type):
    global dealer_hand
    c = card[0]
    if c == "A":
        if hand_type == "player":
            card_value = int(input("You have an Ace. Do you want it to be worth 1 or 11? "))
            while card_value != 1 and card_value != 11:
                card_value = int(input("Invalid choice. Enter 1 or 11."))
        elif hand_type == "dealer":
            hand_total = get_hand_total(dealer_hand, hand_type, True)
            if hand_total >= 11:
                card_value = 1
            else:
                card_value = 11
    elif c == "1" or c == "J" or c == "Q" or c == "K":
        card_value = 10
    else:
        card_value = int(c)
    return card_value


def get_status(hide_hole_card, recalculate_player_total):
    global player_hand, dealer_hand, player_total
    print("\nYour cards:")
    for card in player_hand:
        print(" - " + card)
    if recalculate_player_total:
        player_total = get_hand_total(player_hand, "player")
    print("Your total: " + str(player_total))

    if hide_hole_card:
        print("\nDealer has a " + dealer_hand[0] + " and a hidden card.\n\n")
    else:
        print("\nDealer's cards:")
        for card in dealer_hand:
            print(" - " + card)
        print("Dealer's total: " + str(get_hand_total(dealer_hand, "dealer")) + "\n")

    return player_total


def check_winner():
    global player_hand, dealer_hand, bank, bet_amount
    player_total = get_hand_total(player_hand, "player")
    dealer_total = get_hand_total(dealer_hand, "dealer")
    if dealer_total > 21:
        print("\nDEALER BUSTS! YOU WIN! :D\n")
        bank += bet_amount*2
    elif player_total <= 21 and player_total > dealer_total:
        print("\nYOU WIN! :D\n")
        bank += bet_amount*2
    elif player_total == dealer_total:
        print("\nTIE. Nobody wins.\n")
    else:
        print("\nYOU LOSE. :(\n")
        bank -= bet_amount
    reset_game()
    menu()


def reset_game():
    global player_hand, dealer_hand, player_total
    player_hand = []
    dealer_hand = []
    player_total = 0
    createDeck()
    shuffleDeck()


main()
